import React, { Component } from 'react';
import logo from '../../assets/logo.svg';
import './App.css';
import Navbar from '../navbar/Navbar';
import Content from '../content/Content';
import PropTypes from 'prop-types';
import routes from '../../routes/routes';

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };
  render() {
    const { children } = this.props;
    console.log(routes);
    return (
      <div className="App">
        <Navbar title="Navbar" routes={routes}></Navbar>
        <Content body={children}></Content>
      </div>
    );
  }
}

export default App;
