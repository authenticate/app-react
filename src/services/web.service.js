export function authHeader() {
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}

export function logout() {
    localStorage.removeItem('user');
}

export function getToken() {
    return localStorage.getItem('user');
}
