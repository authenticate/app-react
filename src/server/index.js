import express from 'express';
import webpack from 'webpack';
import path from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import open from 'open';

import webpackConfig from '../../webpack.config.babel';

const port = 3200;
const app = express();

app.use(express.static(path.join(__dirname, '../public')));

const isDevelopment = process.env.NODE_ENV !== 'production';

const webpackCompiler = webpack(webpackConfig);
if(isDevelopment) {
    app.use(webpackDevMiddleware(webpackCompiler));
    app.use(webpackHotMiddleware(webpackCompiler));
}
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/index.html'));
});
app.listen(port, err => {
    if (!err) {
        open(`http://localhost:${port}`);
    }
})
