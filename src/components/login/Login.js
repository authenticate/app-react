import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Login.css';
import { withFormik, Field, ErrorMessage, Form } from 'formik';
import { UserService } from '../../services/user.service';
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
class Login extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container">
                <div className="d-flex justify-content-center h-100">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="d-flex justify-content-start">Log In with</h3>
                            <div className="d-flex justify-content-end social_icon">
                                <span><i className="fab fa-facebook-square"></i></span>
                                <span><i className="fab fa-google-plus-square"></i></span>
                                <span><i className="fab fa-twitter-square"></i></span>
                            </div>
                        </div>
                        <div className="card-body">
                            <Form name="form">
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text"><i className="fas fa-user"></i></span>
                                    </div>
                                    <Field name="username" type="text" className="form-control" placeholder="username*"></Field>
                                    <ErrorMessage name="username">
                                        {message => <div class="error-message d-flex">{message}</div>}
                                    </ErrorMessage>
                                </div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text"><i className="fas fa-unlock-alt"></i></span>
                                    </div>
                                    <Field name="password" type="password" className="form-control" placeholder="password*"></Field>
                                    <ErrorMessage name="password">
                                        {message => <div class="error-message d-flex">{message}</div>}
                                    </ErrorMessage>
                                </div>
                                <div className="input-group form-group row aling-items-center remember">
                                    <Field name="rememberMe" type="checkbox"></Field> Remember Me
                                    </div>
                                <div className="input-group form-group">
                                    <button className="form-control color-button" type="submit">LOGIN</button>
                                </div>
                            </Form>
                        </div>
                        <div className="card-footer">
                            <div className="d-flex justify-content-center links">
                                Don't have an account?<Link to="/signup" className="color-text">Sign Up</Link>
                            </div>
                            <div className="d-flex justify-content-center">
                                <a href="#" className="color-text">Forgot password?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withFormik({

    mapPropsToValues(props) {
        return {
            username: '',
            password: '',
            rememberMe: false
        };
    },

    validate(values) {
        const errors = {};

        if (!values.username) {
            errors.username = 'User name is required.';
        }

        if (!values.password) {
            errors.password = 'Password is required.';
        } else if (values.password.length < 3) {
            errors.password = 'Password must be at least 3 characters.';
        }
        return errors;
    },

    handleSubmit(values, formikBag) {
        formikBag.setSubmitting(false);
        // console.log(values);
        // UserService.showConsole(values);
        UserService.authenticate(values);
        console.log(localStorage.getItem('user'));
    }

})(Login);
