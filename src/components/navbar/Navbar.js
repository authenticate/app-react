import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Navbar.css';

class Navbar extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        routes: PropTypes.array.isRequired
    };
    clickButton() {
        console.log('the button with action');
    }
    render() {
        const { title, routes } = this.props;
        return (
            <div className="navbar-color">
                <nav className="navbar-expand-lg navbar navbar-dark">
                    <div className="navbar-brand">
                        {title}
                    </div>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <div className="nav-link">
                                    <Link to="/">{routes[0].title}</Link>
                                </div>
                            </li>
                            <li className="nav-item">
                                <div className="nav-link">
                                    <Link to="/event">{routes[1].title}</Link>
                                </div>
                            </li>
                            <li className="nav-item">
                                <div className="nav-link">
                                    <Link to='/about'>{routes[2].title}</Link>
                                </div>
                            </li>
                        </ul>
                        <Link to="/login">
                            <button className="button-login" onClick={this.clickButton}>
                                LOGIN
                            </button>
                        </Link>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Navbar;