import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Signup.css';
import { withFormik, Field, ErrorMessage, Form } from 'formik';

function Signup(props) {
    const {
        isSubmitting,
        isValid
    } = props;
    return (
        <div className="container">
            <div className="d-flex justify-content-center h-100">
                <div className="card">
                    <div className="card-header">
                        <h3 className="d-flex justify-content-start">Sign Up with</h3>
                        <div className="d-flex justify-content-end social-icon">
                            <span><i className="fab fa-facebook-square"></i></span>
                            <span><i className="fab fa-google-plus-square"></i></span>
                            <span><i className="fab fa-twitter-square"></i></span>
                        </div>
                    </div>
                    <div className="card-body">
                        <Form>
                            <div className="input-group form-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-user"></i></span>
                                </div>
                                {/* <input type="text" className="form-control" placeholder="First Name"></input> */}
                                <Field name="firstName" type="text" className="form-control" placeholder="first name*"></Field>
                                <ErrorMessage name="firstName">
                                    {message => <div className="error-message d-flex">{message}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="input-group form-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-user"></i></span>
                                </div>
                                {/* <input type="text" className="form-control" placeholder="Last Name"></input> */}
                                <Field name="lastName" type="text" className="form-control" placeholder="last name*"></Field>
                                <ErrorMessage name="lastName">
                                    {message => <div className="error-message d-flex">{message}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="input-group form-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-user"></i></span>
                                </div>
                                {/* <input type="text" className="form-control" placeholder="username" /> */}
                                <Field name="username" type="text" className="form-control" placeholder="login*"></Field>
                                <ErrorMessage name="username">
                                    {message => <div className="error-message d-flex">{message}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="input-group form-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                {/* <input type="email" className="form-control" placeholder="email"></input> */}
                                <Field name="email" type="text" className="form-control" placeholder="email*"></Field>
                                <ErrorMessage name="email">
                                    {message => <div className="error-message d-flex">{message}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="input-group form-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-unlock-alt"></i></span>
                                </div>
                                {/* <input type="password" className="form-control" placeholder="password" /> */}
                                <Field name="password" type="password" className="form-control" placeholder="password*"></Field>
                                <ErrorMessage name="password">
                                    {message => <div className="error-message d-flex">{message}</div>}
                                </ErrorMessage>
                            </div>
                            <div className="input-group form-group">
                                <button className="form-control color-button">Singup</button>
                            </div>
                        </Form>
                    </div>
                    <div className="card-footer">
                        <div className="d-flex justify-content-center">
                            You have an account?<Link to="/login">Log In</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default withFormik({

    mapPropsToValues(props) {
        return {
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: '',
        };
    },
    validate(values) {
        const errors = {};
        if (!values.firstName) {
            errors.firstName = 'First name is required';
        }
        if (!values.lastName) {
            errors.lastName = 'Last name is required';
        }
        if (!values.username) {
            errors.username = 'User name is required';
        }
        if (!values.email) {
            errors.email = 'email is required';
        }
        if (!values.password) {
            errors.password = 'Password is required';
        }
        return errors;
    },
    handleSubmit(values, formikBag) {
        formikBag.setSubmitting(false);
        console.log(values);
    }

})(Signup);