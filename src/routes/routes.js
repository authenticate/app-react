export default [
    {
        title: 'Home',
        url: '/'
    },
    {
        title: 'Event',
        url: '/event'   
    },
    {
        title: 'About',
        url: '/about'
    },
    {
        title: 'login',
        url: '/login'
    },
    {
        title: 'signup',
        url: '/signup'
    }
]