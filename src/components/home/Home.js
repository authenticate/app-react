import React, { Component } from 'react';
import logo from '../../assets/logo.svg';
// import banner2 from '../../assets/banner2.png';
import './Home.css';
class home extends Component {
    render() {
        return (
            <div className="Home">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h1>
                                Create Your Own Beautiful Website
                        </h1>
                        </div>
                        <div className="col-sm-12">
                            <p>
                                Your stunning website is just a few clicks away. It's easy and free with Wix.
                        </p>
                        </div>
                        <div className="col-sm-12">
                            <button className="btn button-start">Start Now</button>
                        </div>
                        <div className="col-sm-12">
                            <img src={logo}></img>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 bg-success">
                            hellow word!
                    </div>
                        <div className="col-sm-6 bg-warning">
                            this warning!
                    </div>
                        <div className="col-sm-6 bg-danger">
                            this warning!
                    </div>
                        <div className="col-sm-6 bg-info">
                            this warning!
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default home;