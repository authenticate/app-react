import React from 'react';
import { Route, Switch } from 'react-router-dom';

import App from '../components/app/App';
import Home from '../components/home/Home';
import Page404 from '../components/page404/Page404'
import About from '../components/about/About';
import Event from '../components/event/Event';
import Login from '../components/login/Login';
import Signup from '../components/singup/Signup';
import AdminEvent from '../components/admin/event/Event';
import AdminUser from '../components/admin/user/User';

const AppRoutes = () =>
    <App>
        <Switch>
            <Route exact path='/' component={Home}></Route>
            <Route exact path='/about' component={About}></Route>
            <Route exact path='/event' component={Event}></Route>
            <Route exact path='/login' component={Login}></Route>
            <Route exact path='/signup' component={Signup}></Route>
            <Route exact path='/admin/event' component={AdminEvent}></Route>
            <Route exact path='/admin/user' component={AdminUser}></Route>
            <Route component={Page404}></Route>
        </Switch>
    </App>;

export default AppRoutes;