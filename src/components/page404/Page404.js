import React, { Component } from 'react';
import './Page404.css';
import { Link } from 'react-router-dom';
class Page404 extends Component {
    render() {
        return (
            <div className="container">
                <div className="d-flex justify-content-center h-100">
                    <div className="card">
                        <span className="text">404</span>
                        <div>The page you are looking for was not found.</div>
                        <Link to="/" className="colot-text">Back to Home</Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default Page404;