import React, { Component } from 'react';
import './User.css';
import { withFormik, Field, ErrorMessage, Form } from 'formik';
import Modal from '../../Modal/Modal';
import { UserService } from '../../../services/user.service';
class User extends Component {

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            users: []
        };
    }

    openModalHandler = () => {
        this.setState({
            isShowing: true
        });
    }

    closeModalHandler = () => {
        this.setState({
            isShowing: false
        });
    }

    componentDidMount() {
        UserService.getAll().
        then(json => {
            this.setState({
                users: json
            })
        });
        // fetch('http://localhost:3000/api/users').
        //     then(response => response.json()).
        //     then(json => {
        //         this.setState({
        //             users: json
        //         })
        //     });
    }

    render() {
        const users = this.state.users;
        // console.log('estos son los usuarios --. ', users);
        // console.log(this.stateUsers.users);
        return (
            <div className="continer">
                <div className="d-flex justify-content-center h-100">
                    <div className="card card-modify">
                        <div className="card-header">
                            <div className="row">
                                <div className="col-sm-6 col-xs-2 d-flex justiy-content-start">
                                    <h3>Users</h3>
                                </div>
                                <div className="col-sm-6 col-xs-2 d-flex justify-content-end">
                                    <div class="md-form my-0">
                                        <input type="text" className="form-control" placeholder="search"></input>
                                    </div>
                                    <button type="button" className="button-card-header">Seacrh</button>
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>FirstName</th>
                                            <th>LastName</th>
                                            <th>login</th>
                                            <th>email</th>
                                            <th>Status</th>
                                            <th>
                                                {this.state.isShowing ? <div onClick={this.closeModalHandler} className="back-drop"></div> : null}

                                                <button name="modal1" className="button-card-header" onClick={this.openModalHandler}>New</button>

                                                <Modal
                                                    name="modal1"
                                                    className="modal1"
                                                    show={this.state.isShowing}
                                                    close={this.closeModalHandler}
                                                    title="New">
                                                    <Form>
                                                        <div className="row">
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <span className="span-left">First Name*</span>
                                                                    {/* <input type="text" className="form-control" placeholder="First Name"></input> */}
                                                                    <Field name="firstName" type="text" className="form-control" placeholder="First Name"></Field>
                                                                    <ErrorMessage name="firstName">
                                                                        {message => <div className="message">{message}</div>}
                                                                    </ErrorMessage>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <span className="span-left">Last Name*</span>
                                                                    {/* <input type="text" className="form-control" placeholder="Last Name"></input> */}
                                                                    <Field name="lastName" type="text" className="form-control" placeholder="Last Name"></Field>
                                                                    <ErrorMessage name="lastName">
                                                                        {message => <div className="message">{message}</div>}
                                                                    </ErrorMessage>
                                                                </div>
                                                            </div>

                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <span className="span-left">email*</span>
                                                                    {/* <input type="email" className="form-control" placeholder="email"></input> */}
                                                                    <Field name="email" type="email" className="form-control" placeholder="email"></Field>
                                                                    <ErrorMessage name="email">
                                                                        {message => <div className="message">{message}</div>}
                                                                    </ErrorMessage>
                                                                </div>
                                                            </div>

                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <span className="span-left">User name*</span>
                                                                    {/* <input type="text" className="form-control" placeholder="login"></input> */}
                                                                    <Field name="username" type="text" className="form-control" placeholder="User name"></Field>
                                                                    <ErrorMessage name="username">
                                                                        {message => <div className="message">{message}</div>}
                                                                    </ErrorMessage>
                                                                </div>
                                                            </div>

                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <span className="span-left">Status</span>
                                                                    {/* <input type="checkbox" className="form-control"></input> */}
                                                                    <Field name="status" type="checkbox" className="form-control"></Field>
                                                                </div>
                                                            </div>

                                                            <div className="col-sm-12">
                                                                <div className="d-flex justify-content-end">
                                                                    <button className="btn-modal" type="submit">
                                                                        Save
                                                                            </button>
                                                                    {/* <button className="btn-modal">
                                                                        Delete
                                                                            </button> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Form>
                                                </Modal>
                                            </th>
                                        </tr>
                                    </thead>
                                    {/* <ul>
                                        {users.map(item => (
                                            <li key="{item.id}">
                                                Name: {item.name}
                                            </li>
                                        ))}
                                    </ul> */}
                                    <tbody>
                                        {users.map(item => {
                                            return [
                                                <tr key="{item.id}">
                                                    <td>{item.firstName}</td>
                                                    <td>{item.lastName}</td>
                                                    <td>{item.username}</td>
                                                    <td>{item.email}</td>
                                                    <td>{item.status}</td>
                                                    <td>
                                                        <button className="btn-icon" title="Edit"><i className="fas fa-pen-square"></i></button>
                                                        <button className="btn-icon" title="Delete"><i className="fas fa-trash-alt"></i></button>
                                                    </td>
                                                </tr>
                                            ]
                                        })};

                                        {/* <tr>
                                            <td>Ayar</td>
                                            <td>Perez Vargas</td>
                                            <td>mos</td>
                                            <td>xhosfirst@gmail.com</td>
                                            <td>true</td>
                                            <td>
                                                <button className="btn-icon"><i className="fas fa-pen-square"></i></button>
                                                <button className="btn-icon"><i className="fas fa-trash-alt"></i></button>
                                            </td>
                                        </tr> */}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withFormik({

    mapPropsToValues(props) {
        return {
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            status: false
        };
    },

    validate(values) {
        const errors = {};
        if (!values.firstName) {
            errors.firstName = 'First name is required';
        }
        if (!values.lastName) {
            errors.lastName = 'Last name is required';
        }
        if (!values.username) {
            errors.username = 'User name is required';
        }
        if (!values.email) {
            errors.email = 'email si required';
        }
        return errors;
    },

    handleSubmit(values, formikBag) {
        formikBag.setSubmitting(false);
        // if(!Object.keys(values)) {
        // formikBag.resetForm();
        console.log(values);
        // }
    },

})(User);